#include <stdio.h>
#include <stdlib.h>

#define nbColonnes 10
#define nbLigne 10

typedef int tab_2Dim[nbLigne][nbLigne];

tab_2Dim grille;

void initialisation(tab_2Dim grille)
{
    for (int i = 0; i < nbLigne; i++){
        for (int j = 0 ; j < nbColonnes; j++){
            grille[i][j] = 0;
        }
    }
    grille[2][3] = 1;
    grille[2][4] = 1;
    grille[2][5] = 1;
    grille[2][6] = 1;
    grille[2][7] = 1;

}

void affichage(tab_2Dim grille)
{
    printf("\t0 1 2 3 4 5 6 7 8 9\n");

    for (int i = 0; i < nbLigne ; i++){
        printf("%d",i);
        printf("\t");
        for (int j = 0 ; j < nbColonnes ; j++){
            if (grille[i][j] == 1) {
                printf("X ");
            } else {
                printf("  "); // 3 espaces pour avoir la même taille que si on affiche X
            }
        }
        printf("\n");
    }
}

void actualisation(tab_2Dim grille_precedente){
    int voisins,cellule;
    tab_2Dim grille_tmp;
    for (int i = 0 ; i < nbLigne ; i++){
        for (int j = 0 ; j < nbColonnes ; j++){
            cellule = grille_precedente[i][j];
            voisins = grille_precedente[i-1][j-1] + grille_precedente[i-1][j] + grille_precedente[i-1][j+1] + grille_precedente[i][j-1] + grille_precedente[i][j+1] + grille_precedente[i+1][j-1] + grille_precedente[i+1][j] + grille_precedente[i+1][j+1];

            if (voisins == 3 || (cellule == 1 && voisins == 2)){
                grille_tmp[i][j] = 1;
            } else if (voisins != 3){
                grille_tmp[i][j] = 0;
            }
        }
    }
    for (int i = 0; i < nbLigne; i++) {
        for(int j = 0; j < nbColonnes; j++) {
            grille_precedente[i][j] = grille_tmp[i][j];
        }
    }
    affichage(grille_tmp);
}

int main()
{
    tab_2Dim plateau;
    int etape = 2; //commence à deux car première étape déjà affichée


    char reponse;

    initialisation(plateau);


    printf("Affichage de la première étape :\n");
    affichage(plateau);

    //On relance le processus tant que l'utilisateur n'écrit pas 'n'
    do{
        if(reponse != '\n')
            printf("[ÉTAPE %d] Voulez-vous continuer ?  : ", etape);
        scanf("%c", &reponse);
        if(reponse == 'o'){
            actualisation(plateau);
            etape++;
        }
    } while(reponse != 'n');

}
